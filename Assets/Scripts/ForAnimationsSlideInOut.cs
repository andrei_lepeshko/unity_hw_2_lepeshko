﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForAnimationsSlideInOut : MonoBehaviour
{
    bool isSlideIn;
    Animation anim;

    void Start()
    {
        isSlideIn = false;
        anim = GetComponent<Animation>();
    }

    public void Slide()
    {
        if(isSlideIn == false)
        {
            anim.Play("SlideIn");
            isSlideIn = true;
        }
        else
        {
            anim.Play("SlideOut");
            isSlideIn = false;
        }
    }
}
