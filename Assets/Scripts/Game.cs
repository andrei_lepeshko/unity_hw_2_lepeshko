﻿using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Game : MonoBehaviour
{
  public static GameState state;
  public enum GameState
  {
    WAIT,
    GAME,
    VICTORY,
    DEFEAT
  }
  public GameObject player_go;
  public GameObject exit_go;
  public GameObject marker_go;
 //public GameObject camera_go;


    float start_game_stamp;

  int game_time = 30;
  
  static float spawn_markers_stamp;
  float spawn_markers_cooldown = 0.5f;
  
  public float left_time;
  //Rect screen_rect = new Rect(0, 0, 60, 60);

    Marker first_marker;
    Color exit_color;
    Exit exit;
    public GameObject player;
    PlayerFollowing player_following;   

    public bool isVictory = false;

    private int timer = 0;

    public float currentTime = 0f;
    float startingTime = 30f;

    [Header("Для UI")]
    public Text statusText;
    public GameObject fadePanel;
    public GameObject guidText;
    public GameObject indicatorPanel;
    public GameObject addSecondText;
    public RawImage arrow_ui;

    void Start()
    {
        state = GameState.WAIT;
        marker_go.SetActive(false);
    
        exit_color = GetRandomColor(1);
  
        first_marker = CreateMarker();
        first_marker.Init(exit_color, first_marker.transform.position, Vector3.zero, this);

        Player player = player_go.AddComponent<Player>();
        //player.cube.color = exit_color;
        exit = exit_go.AddComponent<Exit>();
        exit.Init(exit_color, player);

        //player_following = camera_go.AddComponent<PlayerFollowing>();

        //player_following.Init(player.gameObject);

        start_game_stamp = Time.time;

        currentTime = startingTime;
    }

    /*float GetLeftTime()
    {
        if (state == GameState.WAIT)
        {
            return game_time;
        }
        else
        {

            return Mathf.Max(0, start_game_stamp + game_time - Time.time);
        }
    }*/

      Marker CreateMarker()
      {
            GameObject marker = Instantiate(marker_go);
            marker.SetActive(true);
            return marker.AddComponent<Marker>(); 
      }

    float spawn_radius = 30;
    Rect spawn_rect = Rect.zero;

    // Update is called once per frame
    void Update()
      {
            if(isVictory == true)
            {
                state = GameState.VICTORY;
                return;
            }

            if (first_marker == null)
            {
                state = GameState.GAME;
                guidText.SetActive(false);
            }
            else return;

            if (state == GameState.VICTORY) return;
            else
            {
                currentTime -= 1 * Time.deltaTime;
                //indicatorPanel.GetComponent<Image>().fillAmount -= 1.0f / 29 * Time.deltaTime; ;
                if (currentTime <= 0)
                {
                    currentTime = 0;
                }
            }
    
            if (currentTime == 0)
            {
                fadePanel.SetActive(true);
                statusText.text = "DEFEAT";
                statusText.color = new Color(255, 0, 0);
                GameOver(false);
            }

            left_time = (int)currentTime;    

            if (first_marker != null || spawn_markers_stamp + spawn_markers_cooldown > Time.time) return;
   
            
  
            Vector3 player_pos = player.transform.position;
            Vector3 exit_pos = exit.transform.position;
                   
        /*arrow_ui.enabled = (exit_pos - player_pos).magnitude > spawn_radius;
        Vector3 player_screen_pos = Camera.main.WorldToScreenPoint(player_pos);
        Vector3 exit_screen_pos = Camera.main.WorldToScreenPoint(exit_pos);
        player_screen_pos.z = 0;
        exit_screen_pos.z = 0;
        Vector3 dir_to_exit = exit_screen_pos - player_screen_pos;
        Vector3 exit_arrow_screen_pos = exit_screen_pos - dir_to_exit.normalized * 20;
        //selector_ui.transform.position = exit_screen_pos;

        //arrow_ui.transform.position = new Vector3(Mathf.Clamp(exit_arrow_screen_pos.x, 20, Screen.width - 20), Mathf.Clamp(exit_arrow_screen_pos.y, 20, Screen.height - 20), 0);
        */
        //arrow_ui.transform.right = (exit_screen_pos - arrow_ui.transform.position).normalized;

        spawn_markers_stamp = Time.time;
        spawn_rect.position = new Vector2(player_pos.x - spawn_radius, player_pos.z - spawn_radius);
            spawn_rect.size = new Vector2(spawn_radius * 2, spawn_radius * 2);

            Vector3 start_pos;
            Vector3 direction;
            float random_pos = 2 * spawn_radius * Random.value;

        switch (Random.Range(0, 4))
            {
                case 0:
                start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMax) + Vector3.back * random_pos;
                direction = Vector3.right;
                  break;
                case 1:
                start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMax) + Vector3.back * random_pos;
                direction = Vector3.left;
                  break;
                case 2:
                start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMax) + Vector3.back * random_pos;
                direction = Vector3.back;
                  break;
                default :
                start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMax) + Vector3.back * random_pos;
                direction = Vector3.forward;
                  break;
            }
            Color color = GetRandomColor(0);
            Marker random_marker = CreateMarker();
            random_marker.Init(color, start_pos, direction, this);
      }

      private Color GetRandomColor(int start_idx = 0)
      {
            return new List<Color>{Color.white, Color.red, Color.green,Color.blue}[Random.Range(start_idx, 4)];
      }

  /*void OnGUI() {
    if (IsGame())
    {
      GUILayout.BeginHorizontal();
      GUI.skin.label.alignment = TextAnchor.UpperLeft;
      GUI.skin.label.normal.textColor = Color.white;
      GUI.Label(new Rect(10, 10, 100, 20), "Left Time: " + left_time.ToString());
      GUI.skin.label.alignment = TextAnchor.UpperRight;
      GUI.skin.label.normal.textColor = exit_color;
      GUI.Label(new Rect(Screen.width - 410, 10, 400, 20), "Increase color intensity(small cubes) for exit(big one)!");
      GUILayout.EndHorizontal();
    }
    else
    {
      GUI.BeginGroup (new Rect (Screen.width / 2 - 50, Screen.height / 2 - 50, 100, 100));
      GUI.Box (new Rect (0,0,100,100), state == GameState.VICTORY ? "VICTORY" : "DEFEAT");
      if (GUI.Button(new Rect(10, 40, 80, 30), "Play again!"))
      {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
      }
      GUI.EndGroup ();
    }
  }
  */

      public static bool IsGame()
      {
            return state == GameState.GAME || state == GameState.WAIT;
      }

      public static void GameOver(bool is_victory)
      {
            state = is_victory ? GameState.VICTORY : GameState.DEFEAT;
      }

      public void LoadScene()
      {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
      }

      public void ForUI()
      {
        addSecondText.SetActive(true);
        Invoke("Fade", 2f);
      }

      public void Fade()
      {
            addSecondText.SetActive(false);
      }
}
