﻿using UnityEngine;

public class PlayerFollowing : MonoBehaviour
{
    float speed = 0.01f;
    GameObject target;
    Vector3 offset;

    public void Init(GameObject target_)
    {
        target = target_;
        offset = transform.position - target.transform.position;
    }

    void LateUpdate()
    {
        Vector3 target_pos = target.transform.position;
        target_pos.y = 0;
        Vector3 desired_pos = target_pos + offset;
        transform.position = Vector3.Lerp(transform.position, desired_pos, speed);
    }
}
