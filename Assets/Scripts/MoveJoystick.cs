﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MoveJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    private Image joystick;
    [SerializeField]
    private Image stick;
    private Vector2 inputVector;
    Cube cube;

    void Start()
    {
        joystick = GetComponent<Image>();
        stick = transform.GetChild(0).GetComponent<Image>();
        cube = FindObjectOfType<Cube>();
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        inputVector = Vector2.zero;
        stick.rectTransform.anchoredPosition = Vector2.zero;
    }

    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 pos;
        if(RectTransformUtility.ScreenPointToLocalPointInRectangle(joystick.rectTransform, ped.position, ped.pressEventCamera, out pos))
        {
            pos.x = (pos.x / joystick.rectTransform.sizeDelta.x);
            pos.y = (pos.y / joystick.rectTransform.sizeDelta.y);
        

            inputVector = new Vector2(pos.x * 2, pos.y * 2);
            inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

            stick.rectTransform.anchoredPosition = new Vector2(inputVector.x * (joystick.rectTransform.sizeDelta.x / 2), inputVector.y * (joystick.rectTransform.sizeDelta.y / 2));

        }
    }

    public float Horizontal()
    {
        if (inputVector.x != Mathf.Clamp(inputVector.x, 0f, 0.45f)) return Mathf.Abs(inputVector.x) / (inputVector.x);
        else return Input.GetAxisRaw("Horizontal");
    }

    public float Vertical()
    {
        if (inputVector.y != Mathf.Clamp(inputVector.y, 0f, 0.45f)) return Mathf.Abs(inputVector.y) / (inputVector.y);
        else return Input.GetAxisRaw("Vertical");
    }

    void Update()
    {
        
    }
}
