﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForHW2 : MonoBehaviour
{
    private Vector3 cameraToObject;
    private Vector3 cameraPosition;
    private Vector3 leftBot;
    private Vector3 rightTop;
    private float distance;
    private Camera followCamera;
    private Vector3 offset = new Vector3(0, 29, 0);

    Game game;
    public Text timerText;
    public Text statusText;
    public GameObject fadePanel;

    public void Start()
    {
        cameraPosition = Camera.main.transform.position;
        followCamera = Camera.main;
        game = FindObjectOfType<Game>();
    }

    public void Update()
    {
        if (Game.state == Game.GameState.WAIT)
        {
            #region Запрет на выход куба-героя из зоны камеры

            cameraToObject = transform.position - cameraPosition;
            distance = -Vector3.Project(cameraToObject, Camera.main.transform.forward).y;

            leftBot = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
            rightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, distance));

            float x_left = leftBot.x;
            float x_right = rightTop.x;
            float z_top = rightTop.z;
            float z_bot = leftBot.z;

            Vector3 clampedPosition = transform.position;
            clampedPosition.x = Mathf.Clamp(clampedPosition.x, x_left, x_right);
            clampedPosition.z = Mathf.Clamp(clampedPosition.z, z_bot, z_top);
            clampedPosition.y = Mathf.Clamp(clampedPosition.y, 0, 1);
            transform.position = clampedPosition;
            #endregion
        }
        else
        {
            Camera.main.transform.position = transform.position + offset;
        }

        if (game.left_time == 10)
        {
            timerText.color = new Color(255, 0, 0);
            game.indicatorPanel.GetComponent<Image>().color = new Color(255, 0, 0);
        }

        timerText.text = game.left_time + " Seconds Left";

        if (game.left_time == 0)
        {
            timerText.enabled = false;
        }
    }

    void OnBecameInvisible()
    {
        //transform.position.x = 0;
    }
}
